use anyhow::Result;
use clap::{Parser, Subcommand};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

mod d75;

mod clone;
mod export;
mod flash;
mod import;
mod info;

#[derive(Subcommand)]
pub enum Commands {
    Ports,
    Info(info::Command),
    Clone(clone::Command),
    Flash(flash::Command),
    Export(export::Command),
    Import(import::Command),
}

#[derive(Parser)]
pub struct Options {
    #[command(subcommand)]
    command: Commands,
}

fn main() -> Result<()> {
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(get_rust_log()))
        .with(tracing_subscriber::fmt::layer())
        .init();

    let options = Options::parse();

    match options.command {
        Commands::Info(cmd) => info::execute(cmd)?,
        Commands::Clone(cmd) => clone::execute(cmd)?,
        Commands::Flash(cmd) => flash::execute(cmd)?,
        Commands::Export(cmd) => export::execute(cmd)?,
        Commands::Import(cmd) => import::execute(cmd)?,
        Commands::Ports => {
            let ports = serialport::available_ports()?;
            for p in ports {
                println!("{}", p.port_name);
            }
        }
    }

    Ok(())
}

fn get_rust_log() -> String {
    std::env::var("RUST_LOG").unwrap_or_else(|_| "info".into())
}
