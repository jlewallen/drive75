use anyhow::Context;
use anyhow::Result;
use serialport::{ClearBuffer, SerialPort};
use std::{io::Write, time::Duration};
use tracing::*;

mod mem;

pub use self::mem::HashMapChannel;
pub use self::mem::Image;
pub use self::mem::MemoryChannel;

pub const DEFAULT_BAUD: u32 = 57_600;

pub struct Radio<T> {
    port: T,
}

impl<T> Radio<T> {
    fn new(port: T) -> Self {
        Self { port }
    }
}

impl Radio<Box<dyn SerialPort>> {
    pub fn from_serial_port(name: &str, baud: u32) -> Result<Self, serialport::Error> {
        Ok(Self::new(
            serialport::new(name, baud)
                .data_bits(serialport::DataBits::Eight)
                .parity(serialport::Parity::None)
                .stop_bits(serialport::StopBits::One)
                .flow_control(serialport::FlowControl::Hardware)
                .timeout(Duration::from_millis(2000))
                .open()?,
        ))
    }

    #[allow(dead_code)]
    pub fn clear(&mut self) -> Result<(), serialport::Error> {
        self.port.clear(ClearBuffer::All)
    }

    pub fn command(&mut self, command: &str) -> Result<String, serialport::Error> {
        let command = format!("{}\r", command);
        let written = self.port.write(command.as_bytes())?;
        assert!(written == command.len());

        let mut buffer: Vec<u8> = vec![0; 1000];
        let read = self.port.read(buffer.as_mut_slice())?;
        trace!("(cmd) {} bytes read", read);
        buffer.truncate(read);

        Ok(String::from_utf8(buffer)
            .expect("utf8 error")
            .trim()
            .to_owned())
    }

    fn read_block(&mut self, number: i16) -> Result<Vec<u8>> {
        let zero: i16 = 0;
        let mut command: Vec<u8> = Vec::new();
        command.write("R".as_bytes())?;
        command.write(&number.to_be_bytes())?;
        command.write(&zero.to_be_bytes())?;

        trace!("(read-block) sending {:?}", command);
        let written = self.port.write(&command)?;
        assert!(written == command.len());

        let mut buffer: Vec<u8> = vec![0; 256 + 5];
        let _read = self
            .port
            .read_exact(buffer.as_mut_slice())
            .with_context(|| "reading block")?;
        trace!("(read-block) {} bytes read", buffer.len());

        // Send ack.
        let mut ack: [u8; 1] = [b'\x06'];
        self.port.write(&ack)?;
        assert!(self.port.read(&mut ack)? == 1);
        assert!(ack[0] == b'\x06');

        // This range trims the header off the block.
        Ok(buffer[5..].into())
    }

    pub fn read_memory(&mut self) -> Result<Image> {
        assert!(self.command("0M PROGRAM")? == "0M");

        let mut data = Vec::new();

        for block in 0..(mem::SIZE / mem::BLOCK_SIZE) {
            let _span = span!(Level::DEBUG, "reading", %block).entered();
            data.extend(self.read_block(block as i16)?);
        }

        self.end_mcp()?;

        Ok(Image::new(data))
    }

    fn write_block(&mut self, number: i16, bytes: &[u8]) -> Result<()> {
        let zero: i16 = 0;
        let mut command: Vec<u8> = Vec::new();
        command.write("W".as_bytes())?;
        command.write(&number.to_be_bytes())?;
        command.write(&zero.to_be_bytes())?;

        trace!(
            "(write-block) sending {:?} and {} bytes",
            command,
            bytes.len()
        );

        let written = self.port.write(&command)?;
        assert!(written == command.len());
        let written = self.port.write(bytes)?;
        assert!(written == bytes.len());
        self.port.flush()?;

        // Read ack.
        let mut ack: [u8; 1] = [b'\x06'];
        assert!(self.port.read(&mut ack)? == 1);
        assert!(ack[0] == b'\x06');

        Ok(())
    }

    pub fn write_memory(&mut self, image: &Image) -> Result<()> {
        assert!(self.command("0M PROGRAM")? == "0M");

        for block in 0..(mem::SIZE / mem::BLOCK_SIZE) {
            let _span = span!(Level::DEBUG, "writing", %block).entered();
            self.write_block(block as i16, image.block(block))?;
        }

        self.end_mcp()?;

        Ok(())
    }

    fn end_mcp(&mut self) -> Result<()> {
        self.port.write("E".as_bytes())?;

        Ok(())
    }
}
