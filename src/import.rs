use anyhow::Result;
use clap::Args;
use csv::{self};
use std::{collections::HashMap, fs::File, path::PathBuf};
use tracing::*;

use crate::d75::{HashMapChannel, Image, MemoryChannel};

#[derive(Args)]
pub struct Command {
    #[arg(short, long)]
    image: PathBuf,
}

pub fn execute(cmd: Command) -> Result<()> {
    let mut reader = csv::Reader::from_reader(std::io::stdin());

    let mut channels = Vec::new();

    for (number, record) in reader
        .deserialize::<HashMap<String, String>>()
        .flatten()
        .filter(|m| m.get("Group").map(|v| !v.is_empty()).unwrap_or_default())
        .enumerate()
    {
        let record: HashMapChannel = HashMapChannel::new(number, record);
        let channel: MemoryChannel = record.try_into()?;
        channels.push(channel);
    }

    channels.sort_by_key(|c| c.number());

    info!("imported {} rows", channels.len());

    let mut image = Image::from_path(&cmd.image)?;
    let mut state = image.state()?;
    state.set_channels(channels);
    image.update(&state)?;

    let mut file = File::create(&cmd.image)?;
    image.write(&mut file)?;

    info!("saved {:?}", cmd.image);

    Ok(())
}
