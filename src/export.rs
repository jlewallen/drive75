use anyhow::Result;
use clap::Args;
use csv;
use std::path::PathBuf;

use crate::d75::{Image, MemoryChannel};

#[derive(Args)]
pub struct Command {
    #[arg(short, long)]
    image: PathBuf,
}

pub fn execute(cmd: Command) -> Result<()> {
    let data = Image::from_path(cmd.image)?;
    let state = data.state()?;

    let mut w = csv::Writer::from_writer(std::io::stdout());
    w.write_record(&MemoryChannel::header())?;
    for channel in state.channels() {
        if channel.used() {
            let record = channel.record();
            w.write_record(&record)?;
        }
    }

    Ok(())
}
