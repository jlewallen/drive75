use anyhow::Result;
use chrono::{DateTime, Utc};
use clap::Args;
use std::{fs::File, path::PathBuf};
use tracing::*;

use crate::d75::{Radio, DEFAULT_BAUD};

#[derive(Args)]
pub struct Command {
    #[arg(short, long)]
    port: String,
    #[arg(short, long)]
    image: Option<PathBuf>,
}

pub fn execute(cmd: Command) -> Result<()> {
    let path = cmd.image.unwrap_or_else(|| {
        let now: DateTime<Utc> = Utc::now();
        now.format("%Y%m%d_%H%M%S.d75").to_string().into()
    });

    // Create the file first, just in case we run into trouble to avoid
    // unnecessarily copying memory.
    let mut file = File::create(&path)?;

    let mut radio = Radio::from_serial_port(&cmd.port, DEFAULT_BAUD)?;
    let data = radio.read_memory()?;

    // Now flush the image to the file.
    data.write(&mut file)?;

    info!("cloned {} bytes to {:?}", data.len(), path);

    Ok(())
}
