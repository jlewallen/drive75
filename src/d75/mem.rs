#![allow(dead_code)]

use binrw::{BinRead, BinReaderExt, BinWrite, BinWriterExt};
use modular_bitfield::prelude::*;
use std::collections::HashMap;
use std::fmt::Display;
use std::io::{Seek, SeekFrom};
use std::num::{ParseFloatError, ParseIntError};
use std::str::{FromStr, ParseBoolError};
use std::{io::Cursor, path::Path};
use thiserror::Error;

// I believe this is page aligned?
pub const SIZE: u32 = 0x7A3CD;
pub const BLOCK_SIZE: u32 = 0x100;
// In Chirp this value is 210, which would overflow into the memory set
// aside for names. I (had) no idea what's going on there. I asked (on the
// wrong mailing list and got scolded) and it's not worrying anybody there.
// I've adjusted my maximum number of channels down to 1000 as that's all I
// intend to use. Note that if the channels do indeed occupy the memory
// right up to the start of the name region, this means there's room for
// 1152 channels.
pub const NUMBER_GROUPS: usize = 192;
pub const NUMBER_CHANNELS: usize = 1000;
// Channel specifics are grouped together and then padded so no channel
// specifics span a page boundary.
pub const CHANNELS_PER_GROUP: usize = 6;
pub const MEMORY_NAMES_OFFSET: u64 = 0x10000;
pub const MEMORY_SPECIFICS_OFFSET: u64 = 0x4000;
pub const MEMORY_FLAGS_OFFSET: u64 = 0x2000;
// 40 bytes per channel * 6 + 16 = 256
pub const MEMORY_GROUP_PADDING: i64 = 16;

#[bitfield]
#[derive(BinRead, BinWrite, Debug)]
#[br(map = Self::from_bytes)]
pub struct Specifics {
    freq: u32,
    offset: u32,

    tuning_step: B4,
    split_tuning_step: B3,
    unknown1: B1,

    unknown2: B1,
    mode: B3,
    narrow: B1,
    fine_mode: B1,
    fine_step: B2,

    tone_mode: B1,
    ctcss_mode: B1,
    dtcs_mode: B1,
    cross_mode: B1,
    unknown3: B1,
    split: B1,
    duplex: B2,

    rtone: u8,

    unknown_ctone: B2,
    ctone: B6,

    unknownd_tcs: B1,
    dtcs_code: B7, // DtcsIndex

    unknown4_0: B2,
    cross_mode_mode: B2,
    unknown4_1: B2,
    dig_squelch: B2,
}

#[derive(BinRead, BinWrite, Debug)]
struct SpecificsRecord {
    specifics: Specifics,
    dv_calls1: [u8; 8],
    dv_calls2: [u8; 8],
    dv_calls3: [u8; 8],
    unknown: u8,
}

#[derive(BinRead, BinWrite, Debug)]
pub enum Used {
    #[brw(magic = 0xffu8)]
    Empty,
    #[brw(magic = 0x00u8)]
    Band2m,
    #[brw(magic = 0x01u8)]
    Band125cm,
    #[brw(magic = 0x02u8)]
    Band70cm,
    #[brw(magic = 0x07u8)]
    BandAir,
}

impl Used {
    pub fn from_frequency(frequency: u32) -> Self {
        // Band-A -TX: 144 - 148, 222 - 225, 430 - 450 MHz
        // RX: 136 - 174, 216 - 260, 410 - 470 MHz
        // Band-B RX: 0.1 - 76, 76 - 108 MHz (WFM)
        match frequency {
            118_000_000..=136_975_000 => Self::BandAir,
            136_000_000..=174_000_000 => Self::Band2m,
            216_000_000..=260_000_000 => Self::Band125cm,
            410_000_000..=470_000_000 => Self::Band70cm,
            _ => Self::Empty,
        }
    }
}

#[derive(Debug, Error)]
pub enum IndexedFieldError {
    #[error("integer error")]
    IntegerError(#[from] ParseIntError),
    #[error("float error")]
    FloatError(#[from] ParseFloatError),
    #[error("index error")]
    IndexError,
}

#[derive(Debug, Default)]
pub struct DtcsCode(u8);

const DTCS_CODES: [u16; 104] = [
    23, 25, 26, 31, 32, 36, 43, 47, 51, 53, 54, 65, 71, 72, 73, 74, 114, 115, 116, 122, 125, 131,
    132, 134, 143, 145, 152, 155, 156, 162, 165, 172, 174, 205, 212, 223, 225, 226, 243, 244, 245,
    246, 251, 252, 255, 261, 263, 265, 266, 271, 274, 306, 311, 315, 325, 331, 332, 343, 346, 351,
    356, 364, 365, 371, 411, 412, 413, 423, 431, 432, 445, 446, 452, 454, 455, 462, 464, 465, 466,
    503, 506, 516, 523, 526, 532, 546, 565, 606, 612, 624, 627, 631, 632, 654, 662, 664, 703, 712,
    723, 731, 732, 734, 743, 754,
];

impl Display for DtcsCode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", DTCS_CODES[self.0 as usize]))
    }
}

impl Into<u8> for DtcsCode {
    fn into(self) -> u8 {
        self.0
    }
}

impl FromStr for DtcsCode {
    type Err = IndexedFieldError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let value: u16 = s.parse()?;
        Ok(Self(
            DTCS_CODES
                .iter()
                .enumerate()
                .find(|(_, v)| **v == value)
                .ok_or(IndexedFieldError::IndexError)?
                .0 as u8,
        ))
    }
}

#[derive(Debug, Default)]
pub struct CtcssTone(u8);

const CTCSS_TONES: [f64; 50] = [
    67.0, 69.3, 71.9, 74.4, 77.0, 79.7, 82.5, 85.4, 88.5, 91.5, 94.8, 97.4, 100.0, 103.5, 107.2,
    110.9, 114.8, 118.8, 123.0, 127.3, 131.8, 136.5, 141.3, 146.2, 151.4, 156.7, 159.8, 162.2,
    165.5, 167.9, 171.3, 173.8, 177.3, 179.9, 183.5, 186.2, 189.9, 192.8, 196.6, 199.5, 203.5,
    206.5, 210.7, 218.1, 225.7, 229.1, 233.6, 241.8, 250.3, 254.1,
];

impl Display for CtcssTone {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", CTCSS_TONES[self.0 as usize]))
    }
}

impl Into<u8> for CtcssTone {
    fn into(self) -> u8 {
        self.0
    }
}

impl FromStr for CtcssTone {
    type Err = IndexedFieldError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let value: f64 = s.parse()?;
        Ok(Self(
            CTCSS_TONES
                .iter()
                .enumerate()
                .find(|(_, v)| **v == value)
                .ok_or(IndexedFieldError::IndexError)?
                .0 as u8,
        ))
    }
}

#[derive(Debug)]
pub struct Mode(u8);

impl FromStr for Mode {
    type Err = ModeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "FM" => Ok(Self(0)),
            "AM" => Ok(Self(2)),
            "LSB" => Ok(Self(3)),
            "USB" => Ok(Self(4)),
            "CW" => Ok(Self(5)),
            "NFM" => Ok(Self(6)),
            "DV" => todo!(),
            _ => Err(ModeError::Unknown),
        }
    }
}

#[derive(Debug, Error)]
pub enum ModeError {
    #[error("unknown mode")]
    Unknown,
}

impl Mode {
    fn label(&self) -> &'static str {
        match self.0 {
            0 => "FM",
            1 => "DV",
            2 => "AM",
            3 => "LSB",
            4 => "USB",
            5 => "CW",
            6 => "NFM",
            7 => "DV",
            _ => "Unknown",
        }
    }
}

impl Into<u8> for Mode {
    fn into(self) -> u8 {
        self.0
    }
}

impl Display for Mode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.label())
    }
}

#[bitfield]
#[derive(BinRead, BinWrite, Debug)]
#[br(map = Self::from_bytes)]
struct Flags {
    unknown1: B7,
    locked: B1,
}

#[derive(BinRead, BinWrite, Debug)]
struct FlagsRecord {
    used: Used,
    flags: Flags,
    group: u8,
    unknown2: u8,
}

#[derive(BinRead, BinWrite, Debug, Default)]
struct NameRecord {
    name: [u8; 16],
}

impl NameRecord {
    pub fn new(name: &str) -> Self {
        let mut record = Self::default();
        record.set_name(name);
        record
    }

    pub fn set_name(&mut self, name: &str) {
        let max_len = self.name.len();
        self.name.fill(0x00);
        if name.len() > max_len {
            self.name.copy_from_slice(&name.as_bytes()[0..max_len]);
        } else {
            self.name[0..name.len()].copy_from_slice(&name.as_bytes());
        }
    }
}

#[derive(Debug)]
pub struct Frequency(u32);

impl Display for Frequency {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self.0))
    }
}

#[derive(Debug)]
pub struct MemoryChannel {
    number: usize,
    flags: FlagsRecord,
    name: NameRecord,
    specifics: SpecificsRecord,
}

impl MemoryChannel {
    pub fn number(&self) -> usize {
        self.number
    }

    pub fn frequency(&self) -> Frequency {
        Frequency(self.specifics.specifics.freq())
    }

    pub fn set_frequency(&mut self, frequency: Frequency) {
        self.specifics.specifics.set_freq(frequency.0);
    }

    pub fn offset(&self) -> Frequency {
        Frequency(self.specifics.specifics.offset())
    }

    pub fn set_offset(&mut self, offset: Frequency) {
        self.specifics.specifics.set_offset(offset.0);
    }

    pub fn name(&self) -> Option<String> {
        String::from_utf8(self.name.name.to_vec()).ok()
    }

    pub fn set_name(&mut self, name: &str) {
        self.name.set_name(name);
    }

    pub fn group(&self) -> u8 {
        self.flags.group
    }

    pub fn locked(&self) -> bool {
        self.flags.flags.locked() > 0
    }

    pub fn tone_mode(&self) -> bool {
        self.specifics.specifics.tone_mode() > 0
    }

    pub fn rx_tone(&self) -> CtcssTone {
        CtcssTone(self.specifics.specifics.rtone())
    }

    pub fn tx_tone(&self) -> CtcssTone {
        CtcssTone(self.specifics.specifics.ctone())
    }

    pub fn dtcs_code(&self) -> DtcsCode {
        DtcsCode(self.specifics.specifics.dtcs_code())
    }

    pub fn mode(&self) -> Mode {
        Mode(self.specifics.specifics.mode())
    }

    pub fn header() -> Vec<String> {
        vec![
            "Number".to_owned(),
            "Name".to_owned(),
            "Frequency".to_owned(),
            "Offset".to_owned(),
            "Group".to_owned(),
            "Locked".to_owned(),
            "Tone".to_owned(),
            "RxTone".to_owned(),
            "TxTone".to_owned(),
            "DTCS".to_owned(),
            "Mode".to_owned(),
        ]
    }

    pub fn record(&self) -> Vec<String> {
        vec![
            format!("{}", self.number),
            format!(
                "{}",
                self.name()
                    .map(|n| n.trim().to_owned())
                    .unwrap_or_else(|| "".to_owned())
            ),
            format!("{}", self.frequency()),
            format!("{}", self.offset()),
            format!("{}", self.group()),
            format!("{}", self.locked()),
            format!("{}", self.tone_mode()),
            format!("{}", self.rx_tone()),
            format!("{}", self.tx_tone()),
            format!("{}", self.dtcs_code()),
            format!("{}", self.mode()),
        ]
    }

    pub fn used(&self) -> bool {
        !matches!(self.flags.used, Used::Empty)
    }
}

pub struct MemoryState {
    channels: Vec<MemoryChannel>,
}

impl MemoryState {
    pub fn set_channels(&mut self, channels: Vec<MemoryChannel>) {
        self.channels = channels
    }

    pub fn channels(&self) -> impl Iterator<Item = &MemoryChannel> {
        self.channels.iter()
    }
}

pub struct Image {
    data: Vec<u8>,
}

impl Image {
    pub fn new(data: Vec<u8>) -> Image {
        Self { data }
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn from_path<P: AsRef<Path>>(path: P) -> std::io::Result<Self> {
        Ok(Self::new(std::fs::read(path)?))
    }

    pub fn from_reader<R: std::io::Read>(reader: &mut R) -> std::io::Result<Self> {
        let mut data = Vec::new();
        reader.read_to_end(&mut data)?;
        Ok(Self { data })
    }

    pub fn write<W: std::io::Write>(&self, writer: &mut W) -> std::io::Result<()> {
        writer.write_all(&self.data)
    }

    pub fn state(&self) -> anyhow::Result<MemoryState> {
        let mut cursor = Cursor::new(&self.data);

        cursor.seek(SeekFrom::Start(MEMORY_FLAGS_OFFSET))?;
        let flags = (0..NUMBER_CHANNELS)
            .into_iter()
            .map(|_| cursor.read_le::<FlagsRecord>())
            .collect::<Result<Vec<_>, _>>()?;

        cursor.seek(SeekFrom::Start(MEMORY_NAMES_OFFSET))?;
        let names = (0..NUMBER_CHANNELS)
            .into_iter()
            .map(|_| cursor.read_le::<NameRecord>())
            .collect::<Result<Vec<_>, _>>()?;

        cursor.seek(SeekFrom::Start(MEMORY_SPECIFICS_OFFSET))?;
        let mut specifics = Vec::new();
        for _group in 0..NUMBER_GROUPS {
            for _channel in 0..CHANNELS_PER_GROUP {
                specifics.push(cursor.read_le::<SpecificsRecord>()?);
            }
            cursor.seek(SeekFrom::Current(MEMORY_GROUP_PADDING))?;
        }

        let flags_and_names = flags.into_iter().zip(names.into_iter());
        let channels: Vec<_> = flags_and_names
            .into_iter()
            .zip(specifics.into_iter())
            .enumerate()
            .map(|(number, ((flags, name), specifics))| MemoryChannel {
                number,
                flags,
                name,
                specifics,
            })
            .collect();

        Ok(MemoryState { channels })
    }

    pub fn update(&mut self, state: &MemoryState) -> anyhow::Result<()> {
        let mut cursor = binrw::io::Cursor::new(&mut self.data);
        cursor.seek(SeekFrom::Start(MEMORY_FLAGS_OFFSET))?;
        for channel in state.channels.iter() {
            cursor.write_le(&channel.flags)?;
        }

        cursor.seek(SeekFrom::Start(MEMORY_NAMES_OFFSET))?;
        for channel in state.channels.iter() {
            cursor.write_le(&channel.name)?;
        }

        cursor.seek(SeekFrom::Start(MEMORY_SPECIFICS_OFFSET))?;
        let mut channels = state.channels.iter();
        'outer: for _group in 0..NUMBER_GROUPS {
            for _channel in 0..CHANNELS_PER_GROUP {
                if let Some(channel) = channels.next() {
                    cursor.write_le::<SpecificsRecord>(&channel.specifics)?;
                } else {
                    break 'outer;
                }
            }
            cursor.seek(SeekFrom::Current(MEMORY_GROUP_PADDING))?;
        }

        Ok(())
    }

    pub fn block(&self, block: u32) -> &[u8] {
        &self.data[block as usize * 256..(block as usize + 1) * 256]
    }
}

#[derive(Debug)]
pub struct FlexBool(bool);

impl FromStr for FlexBool {
    type Err = FromError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "true" | "Y" => Ok(Self(true)),
            "false" | "N" => Ok(Self(true)),
            _ => Ok(Self(false)),
        }
    }
}

impl Into<bool> for FlexBool {
    fn into(self) -> bool {
        self.0
    }
}

#[derive(Debug)]
pub struct HashMapChannel {
    number: usize,
    map: HashMap<String, String>,
}

impl HashMapChannel {
    pub fn new(number: usize, map: HashMap<String, String>) -> Self {
        Self { number, map }
    }
}

#[derive(Debug, Error)]
pub enum FromError {
    #[error("Missing field `{0}`")]
    FieldError(&'static str),
    #[error("Integer format")]
    IntFormatError(#[from] ParseIntError),
    #[error("Boolean format")]
    BoolFormatError(#[from] ParseBoolError),
    #[error("Float format")]
    FloatFormatError(#[from] ParseFloatError),
    #[error("Mode format")]
    ModeError(#[from] ModeError),
    #[error("Indexed field format")]
    IndexedFieldError(#[from] IndexedFieldError),
}

impl TryFrom<HashMapChannel> for MemoryChannel {
    type Error = FromError;

    fn try_from(value: HashMapChannel) -> Result<Self, Self::Error> {
        let number = value.number;
        let mut value = value.map;
        let number = value
            .remove("Number")
            .map(|n| n.parse())
            .map_or(Ok(Some(number)), |v| v.map(Some))?
            .unwrap();
        let name = value.remove("Name").ok_or(FromError::FieldError("Name"))?;
        let frequency = value
            .remove("Frequency")
            .ok_or(FromError::FieldError("Frequency"))?;
        let offset = value.remove("Offset").filter(|v| !v.is_empty());
        let group = value
            .remove("Group")
            .ok_or(FromError::FieldError("Group"))?;
        let locked: FlexBool = value
            .remove("Locked")
            .filter(|v| !v.is_empty())
            .map(|n| n.parse())
            .map_or(Ok(Some(FlexBool(false))), |v| v.map(Some))?
            .unwrap();
        let locked = locked.into();
        let tone: FlexBool = value
            .remove("Tone")
            .filter(|v| !v.is_empty())
            .map(|n| n.parse())
            .map_or(Ok(Some(FlexBool(false))), |v| v.map(Some))?
            .unwrap();
        let tone: bool = tone.into();
        let rx_tone: CtcssTone = value
            .remove("RxTone")
            .filter(|v| !v.is_empty())
            .map(|n| n.parse())
            .map_or(Ok(Some(CtcssTone::default())), |v| v.map(Some))?
            .unwrap();
        let tx_tone: CtcssTone = value
            .remove("TxTone")
            .filter(|v| !v.is_empty())
            .map(|n| n.parse())
            .map_or(Ok(Some(CtcssTone::default())), |v| v.map(Some))?
            .unwrap();
        let dtcs: DtcsCode = value
            .remove("DTCS")
            .filter(|v| !v.is_empty())
            .map(|n| n.parse())
            .map_or(Ok(Some(DtcsCode::default())), |v| v.map(Some))?
            .unwrap();
        let mode: Mode = value
            .remove("Mode")
            .ok_or(FromError::FieldError("Mode"))?
            .parse()?;

        let frequency: f64 = frequency.parse()?;
        let frequency: u32 = (frequency * 1_000_000f64) as u32;

        let offset: u32 = match offset {
            Some(offset) => {
                let offset: f64 = offset.parse()?;
                (offset * 1_000_000f64) as u32
            }
            None => 0,
        };

        let name = NameRecord::new(&name);

        let used = Used::from_frequency(frequency);

        let specifics = Specifics::new()
            .with_freq(frequency)
            .with_offset(offset)
            .with_mode(mode.into())
            .with_fine_mode(
                // I dunno why?
                if matches!(used, Used::BandAir) {
                    true
                } else {
                    false
                }
                .into(),
            )
            .with_tone_mode(tone.into())
            .with_rtone(rx_tone.into())
            .with_ctone(tx_tone.into())
            .with_dtcs_code(dtcs.into());

        fn bool_to_u8(value: bool) -> u8 {
            if value {
                1
            } else {
                0
            }
        }

        Ok(Self {
            number,
            flags: FlagsRecord {
                used,
                flags: Flags::new().with_locked(bool_to_u8(locked)),
                group: group.parse()?,
                unknown2: Default::default(),
            },
            name,
            specifics: SpecificsRecord {
                specifics,
                dv_calls1: Default::default(),
                dv_calls2: Default::default(),
                dv_calls3: Default::default(),
                unknown: Default::default(),
            },
        })
    }
}
