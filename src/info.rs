use anyhow::Result;
use clap::Args;

use crate::d75::Radio;

#[derive(Args)]
pub struct Command {
    #[arg(short, long)]
    port: String,
}

pub fn execute(cmd: Command) -> Result<()> {
    let mut radio = Radio::from_serial_port(&cmd.port, 57_600)?;

    // radio.clear()?;

    println!("{:?}", radio.command("ID")?);
    println!("{:?}", radio.command("TY")?);
    println!("{:?}", radio.command("AE")?);
    println!("{:?}", radio.command("CS")?);
    println!("{:?}", radio.command("GS")?);
    println!("{:?}", radio.command("ME 000")?);

    Ok(())
}
