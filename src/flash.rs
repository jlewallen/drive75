use anyhow::Result;
use clap::Args;
use std::path::PathBuf;
use tracing::*;

use crate::d75::{Image, Radio, DEFAULT_BAUD};

#[derive(Args)]
pub struct Command {
    #[arg(short, long)]
    port: String,
    #[arg(short, long)]
    image: PathBuf,
}

pub fn execute(cmd: Command) -> Result<()> {
    let image = Image::from_path(cmd.image)?;
    let mut radio = Radio::from_serial_port(&cmd.port, DEFAULT_BAUD)?;
    radio.write_memory(&image)?;

    info!("flashed {} bytes to {:?}", image.len(), cmd.port);

    Ok(())
}
